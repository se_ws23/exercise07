package side.effects

import scala.collection.mutable.*

class MyClass(var identifier: String, var duration: String):
  def setId(identifier: String): Unit = this.identifier = identifier
  def setDuration(duration: String): Unit = this.duration = duration

  def retrieveSameDuration(l: ListBuffer[String]): ListBuffer[String] =
    var toRemove: ListBuffer[String] = ListBuffer()
    for (x <- l)
      if (!compareTime(x))
        toRemove += x

    for (x <- toRemove)
      l -= x
    l

  private def compareTime(duration: String): Boolean =
    val seconds1 = inSeconds(duration)
    val seconds2 = inSeconds(this.duration)

    if (seconds1.isDefined && seconds2.isDefined)
      seconds1.get == seconds2.get
    else
      false

  private def inSeconds(duration: String): Option[Int] = duration match
    case s if s.toLowerCase.endsWith("second")
           || s.toLowerCase.endsWith("seconds") =>
      Some(s.split(' ').head.toInt)
    case s if s.toLowerCase.endsWith("minute")
           || s.toLowerCase.endsWith("minutes") =>
      Some(s.split(' ').head.toInt*60)
    case s if s.toLowerCase.endsWith("hour")
           || s.toLowerCase.endsWith("hours")   =>
      Some(s.split(' ').head.toInt*60*60)
    case _ => None

object MyClass:
  val STD_IDENTIFIER: String = "-1;unknown;/dev/null"
  val STD_DURATION: String = "0 seconds"
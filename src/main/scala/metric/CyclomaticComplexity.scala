package metric

case class Game(visitorTeam: String, homeTeam: String, visitorScore: Int, homeScore: Int)

object CyclomaticComplexity:
  def statistics(games: List[Game]): String =
    var wins: Map[String, Int] = Map()
    var i: Int = 0

    while (i < games.length) {
      val game: Game = games(i)

      if (game.homeScore > game.visitorScore) {
        val homeName = game.homeTeam
        wins += (homeName -> (wins.getOrElse(homeName, 0) + 1))
      } else if (game.visitorScore > game.homeScore) {
        val visitorName = game.visitorTeam
        wins += (visitorName -> (wins.getOrElse(visitorName, 0) + 1))
      }
      i = i + 1
    }

    val winsList: List[(String, Int)] = wins.toList
    var j: Int = 0
    var winningTeam: String = ""
    var winningScore: Int = 0

    while (j < winsList.length) {
      val (team, score) = winsList(j)
      if (winningTeam.isBlank || score > winningScore) {
        winningTeam = team
        winningScore = score
      }
      j = j + 1
    }

    s"$winningTeam: $winningScore"